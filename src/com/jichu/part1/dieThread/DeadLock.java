package com.jichu.part1.dieThread;

/**
 * @Author Zhang Chao
 * @Date 2021/2/23 20:18
 * @Version 1.0
 */
//死锁：多个线程下相互抱着对方需要的资源，然后形成僵持
public class DeadLock {


    public static void main(String[] args) {
        Makeup p1 = new Makeup(0,"灰姑娘");
        Makeup p2 = new Makeup(1,"白雪");
        p1.start();
        p2.start();


    }
}

//口红
class Lipstick{

}
//镜子
class Mirror{

}
class Makeup extends Thread{

    //需要的资源只有一份  所以使用static 来保证只有一份
    static Lipstick lipstick = new Lipstick();
    static Mirror mirror = new Mirror();

    int choice; //选择
    String girlName; //使用化妆品的人

    Makeup(int choice,String girlName){
        this.choice =  choice;
        this.girlName = girlName;
    }

    @Override
    public void run() {
//        super.run();
        //化妆
        try {
            makeup();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


//    //化妆，有死锁版
//    private void makeup() throws InterruptedException {
//        if(choice==0){
//            synchronized (lipstick){
//                System.out.println(this.girlName+"获得口红的锁");
//                Thread.sleep(1000);
//                synchronized (mirror){
//                    System.out.println(this.girlName+"获得镜子的锁");
//                }
//            }
//        }else{
//            synchronized (mirror){
//                System.out.println(this.girlName+"获得镜子的锁");
//                Thread.sleep(2000);
//                synchronized(lipstick){
//                    System.out.println(this.girlName+"获得口红的锁");
//
//                }
//            }
//        }
//    }
    //化妆，无死锁版
    private void makeup() throws InterruptedException {
        if(choice==0){
            synchronized (lipstick){
                System.out.println(this.girlName+"获得口红的锁");
                Thread.sleep(1000);

                }
            synchronized (mirror){
                System.out.println(this.girlName+"获得镜子的锁");
            }
        }else{
            synchronized (mirror){
                System.out.println(this.girlName+"获得镜子的锁");
                Thread.sleep(2000);
                }

            synchronized(lipstick){
                System.out.println(this.girlName+"获得口红的锁");
            }
        }
    }
}
