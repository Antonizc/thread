package com.jichu.part1.state;

/**
 * @Author Zhang Chao
 * @Date 2021/2/22 17:05
 * @Version 1.0
 */

/*测试 线程优先级:  这里是概率分配  所以以后先设置优先级  再开启线程*/
public class TestPriority {

    public static void main(String[] args){
        System.out.println(Thread.currentThread().getName()+"--->"+Thread.currentThread().getPriority());


        MyPriority myPriority = new MyPriority();

        Thread t1 = new Thread(myPriority);
        Thread t2 = new Thread(myPriority);
        Thread t3 = new Thread(myPriority);
        Thread t4 = new Thread(myPriority);
        Thread t5 = new Thread(myPriority);
        Thread t6 = new Thread(myPriority);

        //先设置优先级  在启动
        t1.start();


        t2.setPriority(1);
        t2.start();


        t3.setPriority(4);
        t3.start();


        t4.setPriority(Thread.MAX_PRIORITY); //10
        t4.start();

        t5.setPriority(Thread.MIN_PRIORITY);//5
        t5.start();

        t6.setPriority(11);
        t6.start();
    }
}

class MyPriority implements Runnable{

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"--->"+Thread.currentThread().getPriority());
    }
}
