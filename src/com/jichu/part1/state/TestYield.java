package com.jichu.part1.state;

/**
 * @Author Zhang Chao
 * @Date 2021/2/22 2:07
 * @Version 1.0
 */

//[测试 13 ]礼让线程
public class TestYield {
//礼让不一定成功,看cpu心情

    public static void main(String[] args){

        MyYieLd myYieLd = new MyYieLd();
        new Thread(myYieLd,"a").start();
        new Thread(myYieLd,"b").start();

    }
}

class MyYieLd implements Runnable{

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"线程开始执行了");
        Thread.yield();
        System.out.println(Thread.currentThread().getName()+"线程停止执行了");
    }
}
