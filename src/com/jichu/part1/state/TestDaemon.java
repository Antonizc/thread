package com.jichu.part1.state;

/**
 * @Author Zhang Chao
 * @Date 2021/2/22 17:57
 * @Version 1.0
 */

//测试守护线程
    //上帝守护者你
public class TestDaemon {

    public static void main(String[] args) {
        God god = new God();
        You you = new You();

        Thread thread = new Thread(god);
        thread.setDaemon(true);//默认是flase表示是用户线程,正常的线程都是用户线程.....
        //这里的原因 是虚拟机关闭需要一定的时间  虚拟机不用等待守护线程执行完毕
        thread.start(); //上帝守护线程启动

        new Thread(you).start(); // 你 用户线程启动
    }
}

//上帝
class God implements Runnable{

    @Override
    public void run() {
        while(true){
            System.out.println("上帝保佑着你");
        }
    }
}

//你
class You implements Runnable{

    @Override
    public void run() {
        for (int i = 0; i < 36500; i++) {
            System.out.println("你一生都很快乐的或者");
        }
        System.out.println("goodbye! world");
    }
}
