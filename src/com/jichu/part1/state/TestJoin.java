package com.jichu.part1.state;

/**
 * @Author Zhang Chao
 * @Date 2021/2/22 8:36
 * @Version 1.0
 */

/*测试线程强制执行 join
*
* join合成线程, 特此线程执行完成之后,再之心其他线程,其他线程阻塞
*
* 想想成为插队
* */
public class TestJoin implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("线程vip来了");
        }
    }

    public static void main(String[] args) throws InterruptedException {
        //启动我们的线程
        TestJoin testJoin =  new TestJoin();
        Thread thread = new Thread(testJoin);
        thread.start();

        //主线程
        for (int i = 0; i < 500; i++) {
            if(i==200){
                thread.join();
            }
            System.out.println("main"+i);
        }
    }
}
