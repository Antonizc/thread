package com.jichu.part1.state;

/**
 * @Author Zhang Chao
 * @Date 2021/2/21 23:59
 * @Version 1.0
 */
/**
 *  要让线程自己停止下来
 * */
public class TestStop implements Runnable{

    //1.设置一个标识位
    private boolean flag = true;

    @Override
    public void run() {
        int i = 0 ;
        while(flag){
            System.out.println("run....Thread"+i++);
        }
    }

    //2.设置一个公开的方法 停止线程  原理:转换标志位
    public void stop(){
        this.flag = false;
    }

    public static void main(String[] args) {

        TestStop testStop = new TestStop();
        new Thread(testStop).start();

        for (int i = 0; i < 1000; i++) {
            System.out.println("main"+i);
            if(i==900){
                testStop.stop();
                System.out.println("线程该停止了");
            }
        }
    }
}
