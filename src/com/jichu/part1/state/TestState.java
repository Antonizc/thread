package com.jichu.part1.state;

/**
 * @Author Zhang Chao
 * @Date 2021/2/22 13:51
 * @Version 1.0
 */
public class TestState {

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(()->{
            for (int i = 0; i < 5; i++) {//让thread这个线程 先停止5s
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("/////");
        });

        //观察状态
        Thread.State state = thread.getState();
        System.out.println(state);   //new

        //观察启动后
        thread.start();
        state = thread.getState();
        System.out.println(state); //runnable 正在运行

        //只要线程不终止,就一直输出状态
        while(state != Thread.State.TERMINATED){

            Thread.sleep(100);
            state = thread.getState();
            System.out.println(state);
        }

        state = thread.getState();
        System.out.println(state);
//        thread.start();  线程死亡之后 就不能在启动了  也就是说 线程不能启动两次
    }
}
