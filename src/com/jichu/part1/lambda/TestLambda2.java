package com.jichu.part1.lambda;

/**
 * @Author Zhang Chao
 * @Date 2021/2/21 22:13
 * @Version 1.0
 */
public class TestLambda2 {
    public static void main(String[] args) {

        /*带参 lambda*/
        lLove loves = (int a)->{
            System.out.println("i love you-->"+a);
        };

        /*带参简化 lambda*/
        loves = (a)->{
            System.out.println("i love you-->"+a);
        };

        /*简化2 去括号*/
        loves = a->{
            System.out.println("i love you-->"+a);
        };
        /*简化3 去花括号*/
        loves = a-> System.out.println("i love you-->"+a);

        /**
         * 总结
         * lambda 表达式   只有一行代码的时候 可以简化为一行  多行 用代码块{}
         * 接口时函数式接口
         * 多个参数 可以去掉类型都去掉  但是要加括号
         */


        loves.love(520);
    }
}

interface lLove{
    void love(int a);
}