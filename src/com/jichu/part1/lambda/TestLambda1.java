package com.jichu.part1.lambda;

/**
 * @Author Zhang Chao
 * @Date 2021/2/21 21:07
 * @Version 1.0
 */
/*推到 lambda 表达式*/
public class TestLambda1 {

    //3.静态内部类
     static class Like2 implements ILike{
        @Override
        public void lambda() {
            System.out.println(" i like lambda2");
        }
    }

    public static void main(String[] args) {
        ILike like = new Like();
        like.lambda();

        like = new Like2();
        like.lambda();

        //4.局部内部类
        class Like3 implements ILike{
            @Override
            public void lambda() {
                System.out.println(" i like lambda3");
            }
        }
        like = new Like3();
        like.lambda();

        //5.匿名内部类  没有类名 必须使用接口或者父类
        like = new ILike(){

            @Override
            public void lambda() {
                System.out.println(" i like lambda4");
            }
        };
        like.lambda();

        //6.用lambda简化  省略了匿名内部类   前提:任何接口  只包含一个抽象方法   这样子写就可以是一个函数式接口
        like = ()-> {
            System.out.println(" i like lambda5");
        };
        like.lambda();


    }
}


//1.定义一个函数式接口
interface ILike{
    void lambda();
}
//2.实现类
class Like implements ILike{
    @Override
    public void lambda() {
        System.out.println(" i like lambda");
    }
}