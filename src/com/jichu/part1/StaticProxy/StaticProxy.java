package com.jichu.part1.StaticProxy;

/**
 * @Author Zhang Chao
 * @Date 2021/2/21 17:18
 * @Version 1.0
 */

/**
 * 静态代理模式总结
 * 真实对象和代理对象都要实现同一个接口
 * 代理对象要代理真实对象
 *
 * 好处
 * 代理对象可以做的更多
 * 真实对象可以专注的做自己的事情
* */
public class StaticProxy {

    public static void main(String[] args) {
        WeddingCompany weddingCompany = new WeddingCompany(new You());
        weddingCompany.HappyMarray();
    }
}
interface Marry{
    void HappyMarray();
}

//真实角色 自己结婚
class You implements Marry{

    @Override
    public void HappyMarray() {
        System.out.println("张超要结婚了,超级开心");
    }
}
//代理角色 帮你结婚
class WeddingCompany implements Marry{

    private Marry target;

    public WeddingCompany(Marry target){
        this.target = target;
    }
    @Override
    public void HappyMarray() {
        before();
        this.target.HappyMarray();
        after();
    }

    private void after() {
        System.out.println("结婚后,收尾款");

    }

    private void before() {
        System.out.println("结婚前,布置现场");
    }
}
