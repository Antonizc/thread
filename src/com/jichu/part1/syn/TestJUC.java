package com.jichu.part1.syn;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @Author Zhang Chao
 * @Date 2021/2/23 17:04
 * @Version 1.0
 */

//测试juc包下的  安全的线程包 java.util.concurrent*   并发的
public class TestJUC {

    public static void main(String[] args){
        CopyOnWriteArrayList<String> list = new CopyOnWriteArrayList<String>();
        for (int i = 0; i < 10000; i++) {
            new Thread(()->{
                list.add(Thread.currentThread().getName());
            }).start();
        }

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(list.size());
    }
}
