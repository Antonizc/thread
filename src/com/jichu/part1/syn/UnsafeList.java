package com.jichu.part1.syn;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Zhang Chao
 * @Date 2021/2/23 1:19
 * @Version 1.0
 */

//线程不安全的集合  list
public class UnsafeList {

    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        for (int i = 0; i < 10000; i++) {

            new Thread(()->{
               list.add(Thread.currentThread().getName());
            }).start();
        }

//        try {
//            Thread.sleep(3000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        System.out.println(list.size());
    }
}
