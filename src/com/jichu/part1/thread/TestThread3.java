package com.jichu.part1.thread;

/**
 * @Author Zhang Chao
 * @Date 2021/2/20 1:33
 * @Version 1.0
 *
 * 创建线性方式2： 实现runnable接口
 *               重写run方法，
 *               执行线程需要丢下runnable接口实现类。
 *               调用start方法
 */


public class TestThread3 implements Runnable{

    public static void main(String[] args) {

        //创建runnable接口的实现类对象
        TestThread3 testThread3 = new TestThread3();

        //创建线程对象，通过线程对象赖启动我们的线程，代理
//        [完整版]
//        Thread thread = new Thread(testThread3);
//        thread.start();

//        【简化版】
        new Thread(testThread3).start();
        for(int i = 0; i < 200 ; i++){
            System.out.println("我在学习多线程********"+i);
        }
    }

    @Override
    public void run() {
        //run方法线程体
        for(int i = 0; i < 100; i++){
            System.out.println("我在看代码********"+i);
        }
    }
}
