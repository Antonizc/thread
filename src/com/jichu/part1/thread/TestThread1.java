package com.jichu.part1.thread;

/**
 * @Author Zhang Chao
 * @Date 2021/2/19 19:47
 * @Version 1.0
 *
 * 【多线程】:继承Thread类
 *          重写run()方法
 *          调用start开启线程
 * 总结：注意，线程开启不一定立刻执行，由cpu调度执行
 */
public class TestThread1 extends Thread{

    @Override
    public void run() {
        for(int i = 0; i < 100; i++){
            System.out.println("我在看代码********"+i);
        }
    }

    public static void main(String[] args){
        //main线程 主线程

        //创建一个线程对象
        TestThread1 testThread1 = new TestThread1();

        //调用start()方法开启线程
        testThread1.start();

        for(int i = 0; i < 200 ; i++){
            System.out.println("我在学习多线程********"+i);
        }
    }
}
