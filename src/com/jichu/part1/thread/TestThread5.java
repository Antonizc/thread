package com.jichu.part1.thread;

/**
 * @Author Zhang Chao
 * @Date 2021/2/21 12:01
 * @Version 1.0
 */

//多个线程同时擦做同一个对象
    //【买火车票的例子】
    /**
     * 发现问题  资源冲突
     * */
public class TestThread5 implements Runnable{

    //票数
    private int ticketNums = 10 ;

//    @Override
//    public void run() {
//        while(true){
//            if (ticketNums<=0){
//                break;
//            }
//            System.out.println(Thread.currentThread().getName()+"-->拿到了第"+ticketNums--+'票');
//        }
//    }


    @Override
    public void run() {
        while(true){
            if (ticketNums<=0){
                break;
            }
            try {
//                模拟延时
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"-->拿到了第"+ticketNums--+'票');
        }
    }

    public static void main(String[] args) {

        TestThread5 tickte = new TestThread5();

        new Thread(tickte,"张超").start();
        new Thread(tickte,"老师").start();
        new Thread(tickte,"黄牛党").start();

    }
}
