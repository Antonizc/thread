package com.jichu.part1.thread;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * @Author Zhang Chao
 * @Date 2021/2/20 1:33
 * @Version 1.0
 */
//练习Thread ，实现多线程同步下载图片
public class TestThread4 implements Runnable{

    private String url;  //网络图片地址
    private String name;  //保存的文件名

    public TestThread4(String url,String name){
        this.url = url;
        this.name = name;
    }

    //下载图片线程的执行体
    @Override
    public void run() {
    WebDownloader webDownloader = new WebDownloader();
    webDownloader.downloader(url,name);
    System.out.println("下载了文件名为："+name);
    }

    public static void main(String[] args){
        TestThread4 t4 = new TestThread4("https://www.baidu.com/img/flexible/logo/plus_logo_web_white_2.png","4.png");
        TestThread4 t5 = new TestThread4("https://www.baidu.com/img/flexible/logo/plus_logo_web_white_2.png","5.png");
        TestThread4 t6 = new TestThread4("https://www.baidu.com/img/flexible/logo/plus_logo_web_white_2.png","6.png");

        new Thread(t4).start();
        new Thread(t5).start();
        new Thread(t6).start();
    }
}

