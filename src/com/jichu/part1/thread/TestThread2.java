package com.jichu.part1.thread;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * @Author Zhang Chao
 * @Date 2021/2/20 1:33
 * @Version 1.0
 */
//练习Thread ，实现多线程同步下载图片
public class TestThread2 extends Thread{

    private String url;  //网络图片地址
    private String name;  //保存的文件名

    public TestThread2(String url,String name){
        this.url = url;
        this.name = name;
    }

    //下载图片线程的执行体
    @Override
    public void run() {
    WebDownloader webDownloader = new WebDownloader();
    webDownloader.downloader(url,name);
    System.out.println("下载了文件名为："+name);
    }

    public static void main(String[] args){
        TestThread2 t1 = new TestThread2("https://www.baidu.com/img/flexible/logo/plus_logo_web_white_2.png","1.png");
        TestThread2 t2 = new TestThread2("https://www.baidu.com/img/flexible/logo/plus_logo_web_white_2.png","2.png");
        TestThread2 t3 = new TestThread2("https://www.baidu.com/img/flexible/logo/plus_logo_web_white_2.png","3.png");

        t1.start();
        t2.start();
        t3.start();
    }
}

//下载器
class WebDownloader{
    public void downloader(String url,String name){
        try {
            FileUtils.copyURLToFile(new URL(url),new File(name));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IO异常，downloader方法出现异常");
        }
    }
}
