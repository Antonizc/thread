package com.jichu.part1.gaoji;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author Zhang Chao
 * @Date 2021/2/23 22:56
 * @Version 1.0
 */
public class TestLock {

    public static void main(String[] args) {
        TestLock2 testLock2 = new TestLock2();
        new Thread(testLock2).start();
        new Thread(testLock2).start();
        new Thread(testLock2).start();
    }
}

class TestLock2 implements Runnable{

    int ticketNums = 10;

    //定义lock锁
    private final ReentrantLock Lock = new ReentrantLock();


    @Override
    public void run() {
        while(true){
            try {
                Lock.lock();

                if(ticketNums>0){
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(ticketNums--);
                }else{
                    break;
                }

            }finally {
                Lock.unlock();
            }


        }
    }
}
