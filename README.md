# Thread

#### 介绍
通过狂神说学习的多线程详解的代码

#### 教学视频
https://www.bilibili.com/video/BV1V4411p7EF?p=12

#### 软件架构

#### 安装教程
1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明
 1. :继承Thread类：com/jichu/part1/thread/TestThread1.java 
 2. :Thread多线程实现网图下载：com/jichu/part1/thread/TestThread2.java 
 3. :实现Runnable接口：com/jichu/part1/thread/TestThread3.java 
 4. :Runnable实现网图下载：com/jichu/part1/thread/TestThread4.java 
 5. 初步认识并发问题产生的不合理问题：com/jichu/part1/thread/TestThread5.java 
 6. 实现龟兔赛跑案例：com/jichu/part1/thread/Race.java 
 7. 实现Callable接口实现多线程：com/jichu/part1/callable/TestCallable.java 
 8. 静态代理模式讲解：com/jichu/part1/StaticProxy/StaticProxy.java 
 9. Lamda表达式的学习：com/jichu/part1/lambda/TestLambda1.java 
 10. 线程停止：com/jichu/part1/state/TestStop.java 
 11. 线程休眠sleep（模拟倒计时/打印当前时间）：com/jichu/part1/state/TestSleep2.java 
 13. 线程礼让：com/jichu/part1/state/TestYield.java 
 14. :线程强制执行 join : com.jichu.part1.state.TestJoin 
 15. :观测线程状态: com.jichu.part1.state.TestState 
 16. :线程优先级: com/jichu/part1/state/TestPriority.java 
 17. :守护线程(daemon):com/jichu/part1/state/TestDaemon.java 后台记录操作日志,监控内容,垃圾回收

 18. :线程同步: 多个线程同时操作 
 19. :不安全的情况 : 
```
                不安全售票: com/jichu/part1/syn/UnsafeBuyTicket.java 

                不安全取钱: com/jichu/part1/syn/UnsafeBank.java 

                线程不安全集合: com.jichu.part1.syn.UnsafeList 
```
20. :synchronized方法
 ```
                synchronized块: public synchronized void method(int args){} 

                :com/jichu/part1/synx 

```
21. :juc：com/jichu/part1/syn/TestJUC.java 
22. :死锁的样子：com/jichu/part1/dieThread/DeadLock.java 
23. :lock 显式锁：com/jichu/part1/gaoji/TestLock.java 
24. :生产者消费者问题：
```
                    管程法：com/jichu/part1/PC/TestPC.java 

                    信号灯法：com/jichu/part1/PC/TestPc2.java
``` 
25. :线程池：com/jichu/part1/lianJieChi/TestPool.java

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
